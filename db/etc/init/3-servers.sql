DROP TABLE IF EXISTS "servers";
CREATE TABLE "servers" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" character varying(255) NOT NULL,
    "shortname" character varying(255) UNIQUE NOT NULL,
    "description" character varying(4096),
    "server_parameters" json default '{}',
    "last_modified_at" timestamp,
    "created_at" timestamp,
    "server_type_id" integer,
    CONSTRAINT "server_type_id_fkey" FOREIGN KEY (server_type_id) REFERENCES server_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT NOT DEFERRABLE
) WITH (oids = false);
