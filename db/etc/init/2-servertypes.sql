DROP TABLE IF EXISTS "server_types";
CREATE TABLE "server_types" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" character varying(255) UNIQUE NOT NULL 
) WITH (oids = false);


INSERT INTO "server_types" ("name") VALUES ('rucio');

