#!/bin/bash

set -e

# Get script name
SCRIPT=$(basename "$0")

# Display Help
Help() {
    echo
    echo "$SCRIPT"
    echo
    echo "Description: Filter url for segments."
    echo "Syntax: $SCRIPT <url> [url|proto|user|pass|host|domain|subdomain|port|path]"
    echo "Example: $SCRIPT https://www.example.com subdomain,domain"
    echo
}

# Show help and exit
if [[ $1 == 'help' ]]; then
    Help
    exit
fi

# check params
[[ -z "$2" ]] && { echo "Parameter filter is empty." ; exit 1; }

# get protocol
protocol=$(echo "$1" | grep "://" | sed -e's,^\(.*://\).*,\1,g')

# Remove the protocol
url_no_protocol=$(echo "${1/$protocol/}")

# Use tr: Make the protocol lower-case for easy string compare
protocol=$(echo "$protocol" | tr '[:upper:]' '[:lower:]')

# remove the protocol
url="$(echo ${1/$protocol/})"

# extract the user (if any)
userpass=$(echo "$url_no_protocol" | grep "@" | cut -d"/" -f1 | rev | cut -d"@" -f2- | rev)
pass=$(echo "$userpass" | grep ":" | cut -d":" -f2)
if [ -n "$pass" ]; then
  user=$(echo "$userpass" | grep ":" | cut -d":" -f1)
else
  user="$userpass"
fi

# host, port and path
hostport=$(echo "${url_no_protocol/$userpass@/}" | cut -d"/" -f1)

host=$(echo "$hostport" | cut -d":" -f1)
port=$(echo "$hostport" | grep ":" | cut -d":" -f2)
path=$(echo "$url_no_protocol" | grep "/" | cut -d"/" -f2-)

for f in $(echo $2 | sed "s/,/ /g")
do
    case $f in
        url)
            echo $url
            ;;
        protocol)
            echo $protocol
            ;;
        user)
            echo $user
            ;;
        pass)
            echo $pass
            ;;
        host)
            echo $host
            ;;
        port)
            echo $port
            ;;
        path)
            echo $path
            ;;
    esac
done
