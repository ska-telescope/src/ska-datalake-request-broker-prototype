#!/bin/bash

# wait until IAM is accepting connections
chmod +x ./etc/init/wait-for-it.sh 
chmod +x ./etc/init/parse-url.sh

host=`./etc/init/parse-url.sh $DRB_API_IAM_URL host`
port=`./etc/init/parse-url.sh $DRB_API_IAM_URL port`
./etc/init/wait-for-it.sh -h $host -p $port -t 0 

# generate rsa keypair
mkdir ~/.ssh
if [[ -z $DRB_API_PUBLIC_KEY_PATH || -z $DRB_API_PRIVATE_KEY_PATH ]]; then
    echo "couldn't find sshkeys, generating a new pair..."
    openssl genrsa -out ~/.ssh/api.private.pem 2048
    openssl rsa -in ~/.ssh/api.private.pem -outform PEM -pubout -out ~/.ssh/api.public.pem
    export DRB_API_PUBLIC_KEY_PATH=`realpath ~/.ssh/api.public.pem`
    export DRB_API_PRIVATE_KEY_PATH=`realpath ~/.ssh/api.private.pem`
else
    echo "found sshkeys, continuing..."
fi

# assign schema directory
if [[ -z $DRB_API_SCHEMA_DIR ]]; then
    echo "using schema directory from environment: $DRB_API_SCHEMA_DIR"
    export DRB_API_SCHEMA_DIR="etc/schema"
else
    echo "using default schema directory: /etc/schema"
fi

python3.8 src/api.py