class DatabaseNotFullyDefined(Exception):
    pass


class ProjectGroupNotFound(Exception):
    pass


class UsernameAlreadyExists(Exception):
    pass


class PermissionDenied(Exception):
    pass
