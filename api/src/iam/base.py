import abc
from functools import wraps
import logging
import os

from fastapi.security import OAuth2PasswordBearer


class IAMAdminClientBase():
    def __init__(self):
        pass

    @abc.abstractmethod
    def add_project(self):
        pass

    @abc.abstractmethod
    def add_user(self):
        pass

    @abc.abstractmethod
    def add_user_to_project(self):
        pass

    @abc.abstractmethod
    def get_projects(self):
        pass

    @abc.abstractmethod
    def get_users(self):
        pass


class IAMOAuth2ClientBase():
    def __init__(self):
        self.wellknown = None

    @abc.abstractproperty
    def authorization_endpoint(self):
        if self.wellknown is None:
            raise AttributeError("well known not initialised.")
        return self.wellKnown['authorization_endpoint']

    @abc.abstractproperty
    def introspection_endpoint(self):
        if self.wellknown is None:
            raise AttributeError("well known not initialised.")
        return self.wellKnown['introspection_endpoint']

    @abc.abstractproperty
    def scheme(self):
        if self.wellKnown is None:
            raise AttributeError("well known not initialised.")
        return OAuth2PasswordBearer(tokenUrl=self.token_endpoint)

    @abc.abstractproperty
    def token_endpoint(self):
        if self.wellKnown is None:
            raise AttributeError("well known not initialised.")
        return self.wellKnown['token_endpoint']

    @abc.abstractmethod
    def get_userinfo(self):
        pass
