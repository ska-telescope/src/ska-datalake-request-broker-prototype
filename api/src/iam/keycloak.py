from functools import wraps
import logging
import os
import json

from schema import ProjectRolesSchema
from exceptions import ProjectGroupNotFound, UsernameAlreadyExists
from keycloak import KeycloakOpenID, KeycloakAdmin, KeycloakGetError

from iam.base import IAMAdminClientBase, IAMOAuth2ClientBase


def handle_keycloak_exceptions(func):
    """ Decorator to catch keycloak exceptions. """
    @wraps(func)
    def catch_exceptions(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except KeycloakGetError as e:
            logging.critical("Keycloak error. %s" % e)
            raise e
        except Exception as e:
            logging.critical("Generic error: %s" % type(e).__name__)
            raise e
    return catch_exceptions


class KeycloakAdminClient(IAMAdminClientBase):
    def __init__(self, server_url, realm_name, username, password):
        self.adminClient = KeycloakAdmin(
            server_url=server_url,
            username=username,
            password=password,
            realm_name=realm_name,
            verify=True)

    @handle_keycloak_exceptions
    def add_project(self, name):
        """ Add a project to IAM. """
        # create project group under projects/
        projects = self.adminClient.get_group_by_path(
            path='/projects',
            search_in_subgroups=False)

        self.adminClient.create_group({
            "name": name
        }, parent=projects['id'])

        # create roles as sub groups
        project = self.adminClient.get_group_by_path(
            path='/projects/{}'.format(name),
            search_in_subgroups=True)

        prs = ProjectRolesSchema(
            os.path.join(os.environ['DRB_API_SCHEMA_DIR'], "roles.json"))
        for role in prs.get_available_roles():
            self.adminClient.create_group({
                "name": role['shortname']
            }, parent=project['id'])
        return "success"

    @handle_keycloak_exceptions
    def add_user(self, firstName, lastName, username, password, email):
        if self.adminClient.get_user_id(username) is not None:
            raise UsernameAlreadyExists

        newUserId = self.adminClient.create_user({
            "username": username,
            "enabled": True,
            "firstName": firstName,
            "lastName": lastName,
            "email": email,
            "credentials": [
                {
                    "value": password,
                    "type": "password", }
            ]})
        return "success"

    @handle_keycloak_exceptions
    def add_user_to_project(self, projectName, username, role):
        userId = self.adminClient.get_user_id(username)
        group = self.adminClient.get_group_by_path("/projects/{}/{}".format(
            projectName, role), search_in_subgroups=True)
        if group is None:
            raise ProjectGroupNotFound
        self.adminClient.group_user_add(userId, group['id'])
        return "success"

    @handle_keycloak_exceptions
    def delete_user(self, id):
        """ Delete user from IAM. """
        self.adminClient.delete_user(id)
        return "success"

    @handle_keycloak_exceptions
    def get_projects(self):
        """ List all projects from IAM. """
        projects = self.adminClient.get_group_by_path(
            path='/projects',
            search_in_subgroups=False)['subGroups']
        return projects

    @handle_keycloak_exceptions
    def get_users(self):
        """ List all users from IAM. """
        users = []
        for user in self.adminClient.get_users({}):
            projects = []
            for group in self.adminClient.get_user_groups(user['id']):
                _, project, role = group['path'].lstrip('/').split('/')
                projects.append("{}:{}".format(project, role))
            user['projects'] = projects
            users.append(user)
        return users


class KeycloakOIDCClient(IAMOAuth2ClientBase):
    def __init__(self, server_url, realm_name, client_id):
        self.client = KeycloakOpenID(
            server_url=server_url,
            realm_name=realm_name,
            client_id=client_id)
        self.wellKnown = self.client.well_know()

    def handle_keycloak_exceptions(func):
        """ Decorator to catch keycloak exceptions. """
        @wraps(func)
        def catch_exceptions(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except KeycloakGetError as e:
                logging.critical("Keycloak error. %s" % e)
                return (False, repr(e))
            except Exception as e:
                logging.critical("Generic error. %s" % e)
                return (False, repr(e))
        return catch_exceptions

    @handle_keycloak_exceptions
    def get_userinfo(self, token):
        """ Get userinfo from token. """
        return self.client.userinfo(token)
