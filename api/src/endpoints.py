import datetime
import docker
import json
import logging
import os
import requests
import inspect
from functools import wraps
from typing import Optional, List, Dict
import uuid

from fastapi import FastAPI, HTTPException, status, Depends, Query
from fastapi.middleware.cors import CORSMiddleware


from schema import ProjectRolesSchema
from encryption import RSA
from exceptions import PermissionDenied
from iam.keycloak import KeycloakOIDCClient, KeycloakAdminClient
from models.project import ProjectModelBase, NewProjectModelRequest, ProjectModelResponse
from models.server import NewServerModelRucioRequest, ServerModelDatabaseResponse
from models.user import UserModelBase, NewUserModelRequest, UserModelResponse
import servers

# Instantiate FastAPI() allowing CORS.
#
app = FastAPI()
origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

# IAM endpoints.
#
if os.environ['DRB_API_IAM_NAME'] == 'keycloak':
    IAMOIDCClient = KeycloakOIDCClient(
        server_url='{}/'.format(
            os.environ['DRB_API_IAM_URL'].rstrip('/')),
        realm_name=os.environ['DRB_API_IAM_REALM_NAME'],
        client_id=os.environ['DRB_API_IAM_CLIENT_ID'])
    IAMAdminClient = KeycloakAdminClient(
        server_url='{}/'.format(
            os.environ['DRB_API_IAM_URL'].rstrip('/')),
        realm_name=os.environ['DRB_API_IAM_REALM_NAME'],
        username=os.environ['DRB_API_KEYCLOAK_ADMIN_USER'],
        password=os.environ['DRB_API_KEYCLOAK_ADMIN_PASSWORD'])

# Schema.
#
ROLES = ProjectRolesSchema(
    os.path.join(os.environ['DRB_API_SCHEMA_DIR'], "roles.json"))


def handle_request_exceptions(func):
    """ Catch and return HTTP exceptions for request. """
    @wraps(func)
    def catch_exceptions(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except docker.errors.NotFound as e:
            raise HTTPException(status.HTTP_400_BAD_REQUEST, repr(e)) from e
        except HTTPException as e:
            raise e
        except Exception as e:
            raise HTTPException(status.HTTP_400_BAD_REQUEST,
                                detail="{}".format(e)) from e
    return catch_exceptions


# User routes.
# ------------


@ app.post("/users/", status_code=status.HTTP_200_OK)
@ handle_request_exceptions
def add_user(user: NewUserModelRequest = Depends()):
    """ Add user. """
    return IAMAdminClient.add_user(
        user.firstName,
        user.lastName,
        user.username,
        user.password,
        user.email)


@ app.get("/users/", response_model=List[UserModelResponse],
          status_code=status.HTTP_200_OK)
@ handle_request_exceptions
def list_users():
    """ Get list of users. """
    return IAMAdminClient.get_users()


@ app.delete("/users/{id}", status_code=status.HTTP_200_OK)
@ handle_request_exceptions
def delete_user(id: uuid.UUID):
    """ Delete user with id. """
    return IAMAdminClient.delete_user(id)


# Project routes.
# ---------------


@ app.post("/projects/", status_code=status.HTTP_200_OK)
@ handle_request_exceptions
def add_project(project: NewProjectModelRequest = Depends()):
    """ Add a project. """
    return IAMAdminClient.add_project(project.name)


@ app.get("/projects/", response_model=List[ProjectModelResponse],
          status_code=status.HTTP_200_OK)
@ handle_request_exceptions
def list_projects():
    """ Get list of projects. """
    return IAMAdminClient.get_projects()


@ app.delete("/projects/{id}", status_code=status.HTTP_200_OK)
@ handle_request_exceptions
def delete_project(id: int):
    """ Delete project with a given id. """
    raise HTTPException(status.HTTP_501_NOT_IMPLEMENTED)


@ app.post("/projects/users", status_code=status.HTTP_200_OK)
@ handle_request_exceptions
def add_user_to_project(project_name: str, username: str,
                        role: str = Query(
                            ROLES.get_lowest_weighted_role(field='shortname'),
                            enum=ROLES.get_available_roles(field='shortname'))):
    """ Add a user to a project. """
    return IAMAdminClient.add_user_to_project(project_name, username, role)


# Server routes.
# --------------


@ app.post("/servers/rucio/userpass", status_code=status.HTTP_200_OK)
@ handle_request_exceptions
def add_rucio_server(server: NewServerModelRucioRequest = Depends()):
    """ Add new Rucio server. """

    # Try pull the Rucio client image
    client = docker.from_env()
    client.images.pull(server.rucio_client_image_url)

    params = {
        'rucio_client_image_url': server.rucio_client_image_url,
        'rucio_account': server.rucio_account,
        'rucio_username': server.rucio_username,
        'rucio_password': RSA.encrypt(server.rucio_password.get_secret_value())
    }
    return servers.add_server(server, params=params, stype="rucio")


@ app.get("/servers/", response_model=List[ServerModelDatabaseResponse],
          status_code=status.HTTP_200_OK)
@ handle_request_exceptions
def list_servers():
    """ Get list of servers. """
    return servers.get_servers()


@ app.delete("/servers/{id}", status_code=status.HTTP_200_OK)
@ handle_request_exceptions
def delete_server(id: int):
    """ Delete a server with a given id. """
    return servers.delete_server(id)


# Test routes.
# ------------

# FIXME: should command be part of path?
@ app.get("/test/{server}/{project}/version", status_code=status.HTTP_200_OK)
@ handle_request_exceptions
def run_echo_command(server: str, project: str,
                     token: str = Depends(IAMOIDCClient.scheme)):
    """ Echo the project in a container if permitted. """
    server = servers.get_servers(shortname=server)[0]
    # FIXME: get_servers should be get_servers_by_shortname
    if server['server_type'] == 'rucio':
        image = server['server_parameters']['rucio_client_image_url']
    groups = IAMOIDCClient.get_userinfo(token)['groups']
    for group in groups:
        _, thisProject, thisRole = group.lstrip('/').split('/')
        if project == thisProject:
            # get permissions for this project/role #FIXME: move to schema class
            with open(os.path.join(
                    os.environ['DRB_API_SCHEMA_DIR'], "roles.json")) as f:
                roles = json.load(f)
            for role in roles['project']:
                if role['shortname'] == thisRole:
                    if 'version' in role['permitted_actions']:
                        with open(os.path.join(
                                os.environ['DRB_API_SCHEMA_DIR'], "actions.json")) as f:
                            actions = json.load(f)
                        client = docker.from_env()
                        container = client.containers.run(
                            image, detach=True, tty=True, auto_remove=True)
                        rtn = ""
                        for action in actions[server['server_type']]['version']:
                            # template with j2 #FIXME https://gist.github.com/sevennineteen/4400462
                            cmd = action.replace("{{ project }}", thisProject).replace(
                                "{{ role }}", thisRole)

                            exitCode, std = container.exec_run(
                                cmd, demux=True)
                            stdout = std[0]
                            stderr = std[1]
                            if stdout:
                                rtn = rtn + stdout.decode('UTF-8')

                        container.kill()
                        return rtn
    raise PermissionDenied(
        "You do not have permission to access this resource")
