import datetime
import json
import logging

from db import handle_database_exceptions, get_new_database_connection


@handle_database_exceptions
def add_server(server, params, stype):
    """ Add a server with parameters, <params>, of type <stype>. """
    conn = get_new_database_connection()
    cursor = conn.cursor()

    cursor.execute(
        "SELECT id from drb.server_types WHERE name='{}'".format(stype))
    serverTypeId = cursor.fetchone()[0]

    cursor.execute("INSERT INTO drb.servers(name, shortname, description, "
                   "server_type_id, server_parameters, created_at) "
                   "VALUES (%s, %s, %s, %s, %s, %s) RETURNING id",
                   (server.name, server.shortname, server.description,
                    serverTypeId, json.dumps(params), datetime.datetime.now())
                   )
    newServerId = cursor.fetchone()[0]
    conn.commit()
    cursor.close()
    conn.close()
    return {
        "server_id": newServerId
    }


@handle_database_exceptions
def delete_server(id):
    """ Delete a server with id, <id>."""
    conn = get_new_database_connection()
    cursor = conn.cursor()

    cursor.execute(
        "DELETE FROM drb.servers WHERE id='{}' RETURNING id".format(id))
    deletedServerId = cursor.fetchone()[0]
    conn.commit()
    cursor.close()
    conn.close()
    return {
        "server_id": deletedServerId
    }


@handle_database_exceptions
def get_servers(shortname=None):
    """ List all servers in the database. """
    conn = get_new_database_connection()

    cursor = conn.cursor()
    query = "SELECT s.id, s.name, s.shortname, s.description, st.name, " + \
        "s.server_parameters,s.last_modified_at, s.created_at " + \
        "FROM drb.servers s " + \
        "LEFT JOIN drb.server_types st ON s.server_type_id = st.id "

    if shortname is not None:
        query = query + "WHERE s.shortname = '{}'".format(shortname)

    cursor.execute(query)
    conn.commit()
    rtn = cursor.fetchall()
    if rtn is None:
        return (True, [])
    cursor.close()
    return [
        {
            'id': entry[0],
            'name': entry[1],
            'shortname': entry[2],
            'description': entry[3],
            'server_type': entry[4],
            'server_parameters': entry[5],
            'last_modified_at': entry[6],
            'created_at': entry[7]
        } for entry in rtn
    ]
