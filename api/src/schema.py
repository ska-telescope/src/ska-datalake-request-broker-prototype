import os
import json


class Schema():
    def __init__(self, schemaPath):
        with open(schemaPath) as f:
            self.schema = json.load(f)


class ProjectRolesSchema(Schema):
    def __init__(self, schemaPath):
        super().__init__(schemaPath)

    def get_available_roles(self, field=None):
        for role in self.schema['project']:
            if field is None:
                yield role
            else:
                yield role[field]

    def get_highest_weighted_role(self, field=None):
        highestWeightedRole = None
        highestWeightedRoleValue = None
        for idx, role in enumerate(self.get_available_roles()):
            if idx == 0:
                highestWeightedRole = role
                highestWeightedRoleValue = role['relative_weighting']
            elif role['relative_weighting'] > highestWeightedRoleValue:
                highestWeightedRole = role
                highestWeightedRoleValue = role['relative_weighting']
        if field is None:
            return highestWeightedRole
        else:
            return highestWeightedRole[field]

    def get_lowest_weighted_role(self, field=None):
        lowestWeightedRole = None
        lowestWeightedRoleValue = None
        for idx, role in enumerate(self.get_available_roles()):
            if idx == 0:
                lowestWeightedRole = role
                lowestWeightedRoleValue = role['relative_weighting']
            elif role['relative_weighting'] < lowestWeightedRoleValue:
                lowestWeightedRole = role
                lowestWeightedRoleValue = role['relative_weighting']
        if field is None:
            return lowestWeightedRole
        else:
            return lowestWeightedRole[field]
