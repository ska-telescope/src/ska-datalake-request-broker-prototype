import argparse
import logging
import os
import requests
import tempfile
import time

import uvicorn

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--l", help="log level", default="INFO", type=str)
    args = parser.parse_args()

    logging.basicConfig(
        level=args.l,
        format=("%(asctime)s [%(threadName)-12.12s]" +
                "[%(process)d] [%(levelname)-5.5s] %(message)s"),
        handlers=[
            logging.StreamHandler()
        ],
    )
    try:
        os.environ['DRB_API_IAM_NAME']
        os.environ['DRB_API_IAM_URL']
        os.environ['DRB_API_IAM_REALM_NAME']
        os.environ['DRB_API_IAM_CLIENT_ID']
        os.environ['DRB_API_DB_USER']
        os.environ['DRB_API_DB_PASSWORD']
        os.environ['DRB_API_DB_NAME']
        os.environ['DRB_API_DB_HOST']
        os.environ['DRB_API_DB_PORT']
        os.environ['DRB_API_SCHEMA_DIR']
        os.environ['DRB_API_PUBLIC_KEY_PATH']
        os.environ['DRB_API_PRIVATE_KEY_PATH']

        # FIXME: follow should really use a service account
        # - https://github.com/marcospereirampj/python-keycloak/issues/141
        # - https://github.com/keycloak/keycloak-documentation/blob/master/server_development/topics/admin-rest-api.adoc)
        os.environ['DRB_API_KEYCLOAK_ADMIN_USER']
        os.environ['DRB_API_KEYCLOAK_ADMIN_PASSWORD']

        rootPath = os.environ['DRB_API_ROOT_PATH']
    except KeyError as e:
        logging.critical(
            "A required environment variable (%s) was not found.", e)
        exit(0)

    uvicorn.run("endpoints:app", host='0.0.0.0', port=3000,
                root_path='/{}'.format(rootPath.lstrip('/')),
                log_level=args.l.lower(), reload=True)
