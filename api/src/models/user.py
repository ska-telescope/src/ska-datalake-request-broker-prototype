from pydantic import BaseModel
from typing import List


class UserModelBase(BaseModel):
    username: str


class NewUserModelRequest(UserModelBase):
    firstName: str
    lastName: str
    password: str
    email: str


class UserModelResponse(UserModelBase):
    id: str
    firstName: str
    lastName: str
    email: str
    projects: List
