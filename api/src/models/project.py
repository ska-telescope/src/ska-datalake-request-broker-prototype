from pydantic import BaseModel, Field
from typing import List


class ProjectModelBase(BaseModel):
    name: str


class NewProjectModelRequest(ProjectModelBase):
    pass


class ProjectModelResponse(ProjectModelBase):
    id: str
    path: str
    subGroups: List
