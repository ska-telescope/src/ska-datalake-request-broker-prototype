import datetime
from typing import Optional

from pydantic import BaseModel, SecretStr


class ServerModelBase(BaseModel):
    name: str
    shortname: str
    description: str


class NewServerModelRucioRequest(ServerModelBase):
    rucio_client_image_url: str
    rucio_account: str
    rucio_username: str
    rucio_password: SecretStr


class ServerModelDatabaseResponse(ServerModelBase):
    id: str
    server_type: str
    server_parameters: dict
    last_modified_at: Optional[datetime.datetime]
    created_at: Optional[datetime.datetime]
