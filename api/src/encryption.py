import abc
import base64
import os
import rsa


class Encryption():
    @abc.abstractstaticmethod
    def encrypt(message):
        pass

    @abc.abstractstaticmethod
    def decrypt(message):
        pass


class RSA():
    @staticmethod
    def encrypt(message):
        ''' Encrypt a message using an RSA key. '''
        # Get public key from environment for encryption of secrets.
        with open(os.environ['DRB_API_PUBLIC_KEY_PATH']) as pubKeyFile:
            pubKeyData = pubKeyFile.read()
        pubKey = rsa.PublicKey.load_pkcs1_openssl_pem(pubKeyData)

        return base64.b64encode(rsa.encrypt(message.encode(), pubKey)).decode('ascii')

    @staticmethod
    def decrypt(code):
        ''' Decrypt a message using an RSA key. '''
        # Get private key from environment for decryption of secrets.
        with open(os.environ['DRB_API_PRIVATE_KEY_PATH']) as privKeyFile:
            privKeyData = privKeyFile.read()
        privKey = rsa.PrivateKey.load_pkcs1(privKeyData)

        return rsa.decrypt(base64.b64decode(code.encode('ascii')), privKey).decode()
