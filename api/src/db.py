from functools import wraps
import logging
import os

from fastapi import FastAPI, HTTPException
import psycopg2

from exceptions import DatabaseNotFullyDefined


def handle_database_exceptions(func):
    """ Decorator to catch database exceptions. """
    @wraps(func)
    def catch_exceptions(*args, **kwargs):
        conn = None
        try:
            return func(*args, **kwargs)
        except psycopg2.DatabaseError as e:
            logging.critical("Database error. %s" % e)
            raise HTTPException(status.HTTP_400_BAD_REQUEST, repr(e)) from e
        finally:
            if conn is not None:
                conn.close()
    return catch_exceptions


def get_new_database_connection(
        name=None, user=None, password=None, host=None, port=None):
    """ 
    Return a new psycopg2 instance with either explicit parameters or 
    parameters from the environment.
    """
    try:
        if name is None:
            name = os.environ['DRB_API_DB_NAME']
        if user is None:
            user = os.environ['DRB_API_DB_USER']
        if password is None:
            password = os.environ['DRB_API_DB_PASSWORD']
        if host is None:
            host = os.environ['DRB_API_DB_HOST']
        if port is None:
            port = os.environ['DRB_API_DB_PORT']
    except KeyError:
        raise DatabaseNotFullyDefined

    dbConnectionStr = "dbname='{}' user='{}' password='{}' host='{}' port='{}'".format(
        name, user, password, host, port
    )
    return psycopg2.connect(dbConnectionStr)
