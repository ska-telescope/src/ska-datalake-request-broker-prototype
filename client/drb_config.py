from urllib.parse import quote, urlunsplit

API_URL_PATH = ""


class DrbConfig:
    """."""

    def __init__(
        self,
        host,
        port=80,
        scheme="http",
        auth_token=None,
        upload_timeout=60 * 5,
        download_timeout=60 * 5,
        rest_timeout=30,
        max_retries=5,
    ):
        self.host = host
        self.port = port
        self.scheme = scheme
        self.auth_token = auth_token
        self.upload_timeout = upload_timeout
        self.download_timeout = download_timeout
        self.rest_timeout = rest_timeout
        self.max_retries = max_retries

    @property
    def base_url(self):
        return "{}://{}:{}".format(self.scheme, self.host, self.port)

    def _join_path_segments(self, *path_segments):
        segments = []
        for segment in path_segments:
            segments += segment.strip("/").split("/")

        return quote("/".join(segments))

    def api_url(self, *path_segments):
        return urlunsplit(
            (
                self.scheme,
                "{}:{}".format(self.host, self.port),
                self._join_path_segments(API_URL_PATH, *path_segments),
                "",  # no query data
                "",  # no fragment
            )
        )
