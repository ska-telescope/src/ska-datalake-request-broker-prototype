import logging

import requests
from requests.adapters import HTTPAdapter
from urllib3.util import Retry

from client.drb_client import DrbClient


class Drb:
    def __init__(self, config):
        self.config = config

        # Setup the requests Session
        # Using a session gives us connection pooling and automatic retry behaviour
        self.requests_session = requests.Session()
        adapter = HTTPAdapter(max_retries=Retry(total=self.config.max_retries))
        self.requests_session.mount("https://", adapter)
        self.requests_session.mount("http://", adapter)

        self.client = DrbClient(self.config, self.requests_session)

    def list_servers(self):
        logging.info("Listing servers")
        return self.client.list_servers().json()
