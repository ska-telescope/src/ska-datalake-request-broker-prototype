class DrbClient:
    def __init__(self, config, requests_session):
        self.config = config
        self.requests_session = requests_session

    def _get(self, url, **kwargs):
        kwargs = self._add_default_kwargs(**kwargs)

        response = self.requests_session.get(url, **kwargs)
        response.raise_for_status()
        return response

    def _post(self, url, data=None, json=None, **kwargs):
        kwargs = self._add_default_kwargs(**kwargs)

        response = self.requests_session.post(url, data, json, **kwargs)
        response.raise_for_status()
        return response

    def _put(self, url, data=None, **kwargs):
        kwargs = self._add_default_kwargs(**kwargs)

        response = self.requests_session.put(url, data, **kwargs)
        response.raise_for_status()
        return response

    def _delete(self, url, **kwargs):
        kwargs = self._add_default_kwargs(**kwargs)

        response = self.requests_session.delete(url, **kwargs)
        response.raise_for_status()
        return response

    def _add_default_kwargs(self, **kwargs):
        if "timeout" not in kwargs:
            kwargs["timeout"] = self.config.rest_timeout

        kwargs.setdefault("headers", {})[
            "Authorization"
        ] = self.get_authorization_header()

        return kwargs

    def _deserialize(self, response, schema):
        return schema.load(response.json()).data

    def get_authorization_header(self):
        return "Bearer {}".format(self.config.auth_token)

    def list_servers(self):
        # TODO: Remove trailing slashes from API endpoints?
        response = self._get(self.config.api_url("servers") + "/")
        return response
