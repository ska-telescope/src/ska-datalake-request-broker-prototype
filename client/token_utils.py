import logging
import os
import keycloak

DRB_AUTH_URL = os.getenv("DRB_AUTH_URL", "http://localhost:8080/auth/")
DRB_AUTH_CLIENT_ID = os.getenv("DRB_AUTH_CLIENT_ID", "drb")
DRB_AUTH_REALM_NAME = os.getenv("DRB_AUTH_REALM_NAME", "drb")

def get_token(username: str, password: str):
    """Get keycloak token."""
    logging.info(f"Auth url = {DRB_AUTH_URL}")
    logging.info(f"Getting auth token... {username} - {password}")
    try:
        keycloak_openid = keycloak.KeycloakOpenID(
            DRB_AUTH_URL + "/",
            client_id=DRB_AUTH_CLIENT_ID,
            realm_name=DRB_AUTH_REALM_NAME,
        )
        token = keycloak_openid.token(username=username, password=password)
        logging.info("Successfully obtained auth token.")
    except keycloak.exceptions.KeycloakAuthenticationError as err:
        logging.error("Unable to get auth token. %s", err)
        raise err
    return token