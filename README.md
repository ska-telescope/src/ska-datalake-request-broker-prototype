# SKA Datalake Request Broker Prototype

## Running

```bash
eng@ubuntu:~/ska-datalake-request-broker-prototype$ export DRB_ROOT=path/to/drb/root
eng@ubuntu:~/ska-datalake-request-broker-prototype$ ./etc/init.sh
eng@ubuntu:~/ska-datalake-request-broker-prototype$ drb-local-up
```

## api

The api frontend is available @ http://localhost:3000/docs

## Keycloak

Keycloak frontend is available @ http://localhost:8080. You must point `drb-keycloak` to `localhost` in `/etc/hosts` to enable communication from both inside and outside
the docker network.

### Credentials

These are hardcoded into the realm json `keycloak/etc/drb-realm.json` under the `drb` client section. There are two accounts:

- `admin`/`password`, and
- `test`/`password`

## Adding groups mapper to keycloak

This is actually now part of the drb realm, but keeping for posterity.

---

You need to attach group membership to the userinfo token. This is done by:

- Logging in to keycloak as admin
- Clients > drb > Mappers > Create > Group Membership
- Fill out the form. Claim name should be `groups`. Ensure the `userinfo_token` toggle is checked.
- Verify by Clients > drb > Client Scopes > Evaluate. Select admin user and click evaluate push button. The token is shown under the `Generated User Info` tab.

## Notes

- The api needs to be periodically restarted as the admin token is not refreshed. This results in keycloak Authorization errors being returned for public endpoints.

## TODO

- Refactor `/test/{server}/{project}/version` route.
- Add endpoint illustrating comms with a Rucio instance.
- Administration endpoints are all exposed publically, no auth is required. These need to be restricted.
- `shortnames`, e.g. server, should be UNIQUE postgres field if they're to be used in endpoint URIs.