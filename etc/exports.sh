#!/bin/python3
#
# Exports for the DRB.
#
export DRB_ETC=$DRB_ROOT/etc

export DRB_API_ROOT=$DRB_ROOT/api
export DRB_DB_ROOT=$DRB_ROOT/db
export DRB_KEYCLOAK_ROOT=$DRB_ROOT/keycloak
