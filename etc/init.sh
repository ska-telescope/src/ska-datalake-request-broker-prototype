#!/bin/bash
#
# This script initialises the environment required to run the DRB microservices, 
# creating aliases/exports and populating service specific configuration files 
# from the values specified in etc/config.ini.
#

echo -n "checking \$DRB_ROOT is set... "
if [ -z "${DRB_ROOT}" ]; then
    echo "FAILED"
    return
else
    echo "OK"
fi

echo -n "exporting environment vars... "
if . $DRB_ROOT/etc/exports.sh >> /dev/null 2>&1; then
    echo "OK"
else
    echo "FAILED"
    return
fi

echo -n "creating aliases... "
if . $DRB_ROOT/etc/aliases.sh >> /dev/null 2>&1; then
    echo "OK"
else
    echo "FAILED"
    return
fi

cd $DRB_ROOT
