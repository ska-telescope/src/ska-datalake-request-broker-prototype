#!/bin/bash
#
# Aliases for the DRB.
#
alias drb-reinit='. '$DRB_ETC'/init.sh '$1

alias drb-local-up='drb-reinit && docker-compose -f '$DRB_ROOT'/docker-compose.yml up'
alias drb-local-down='drb-reinit && docker-compose -f '$DRB_ROOT'/docker-compose.yml down'
alias drb-local-build='drb-reinit && docker-compose -f '$DRB_ROOT'/docker-compose.yml build' 

